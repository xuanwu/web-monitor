#!/bin/sh
#-------------------------------------------------------------
# start exec for web monitor
#-------------------------------------------------------------

java -jar spm-web-1.0.0.jar & >/dev/null 2>&1
echo $! > pid
echo "spm-web monitor starts with pid $!"