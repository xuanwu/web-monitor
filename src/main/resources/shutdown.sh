#!/bin/sh
#-------------------------------------------------------------
# terminate exec for web monitor
#-------------------------------------------------------------

APID=./pid

PID=`cat "$APID"`
kill -15 $PID 
echo "Terminate the spm-web monitor with pid $PID"
rm pid

