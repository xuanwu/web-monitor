package com.spm.web.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.spm.web.util.DataQueue;
import com.sprouts.spm_framework.model.UdpModel;
import com.sprouts.spm_framework.utils.LinuxUtils;
import com.sprouts.spm_framework.utils.Logger;

public class UdpExecServiceImp implements BaseExecService {

    private Logger logger = new Logger();

    /**
     * 使用nc命令扫描端口状态，将来还会分析端口流量
     */
    @Override
    public void analytics(Object... objects) {
        UdpModel udpModel = new UdpModel();

        String ip = (String) objects[0];
        int port = (int) objects[1];
        String commandStr = "nc -vuz " + ip + " " + port + " > output.out 2>&1"; // 由于该命令的结果不是输出到标准output，因此转存到临时文件再分析
        long startTime = System.currentTimeMillis();
        LinuxUtils.execWaitCommand(commandStr);
        long endTime = System.currentTimeMillis();

        File file = new File("output.out");
        udpModel.setStatus(0);
        try {
            String line = null;
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((line = reader.readLine()) != null) {
                if (line.contains("succeeded")) {
                    udpModel.setStatus(1);
                } else {
                    udpModel.setStatus(0);
                }
            }
            reader.close();
            file.delete(); // 解析完后删除临时文件
        } catch (FileNotFoundException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }

        udpModel.setIp(ip);
        udpModel.setPort(port);
        udpModel.setMonitorName((String) objects[2]);
        udpModel.setAverage_res_time(startTime - endTime);

        DataQueue.insertToQueue(udpModel);
    }

}
