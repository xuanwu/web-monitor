package com.spm.web.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.spm.web.util.DataQueue;
import com.sprouts.spm_framework.model.TcpModel;
import com.sprouts.spm_framework.utils.LinuxUtils;
import com.sprouts.spm_framework.utils.Logger;

public class TcpExecServiceImp implements BaseExecService {

    private Logger logger = new Logger();

    /**
     * 使用nc命令扫描端口状态，将来还会分析端口流量
     */
    @Override
    public void analytics(Object... objects) {
        TcpModel tcpModel = new TcpModel();

        String ip = (String) objects[0];
        int port = (int) objects[1];
        String commandStr = "nc -vz " + ip + " " + port + " > output.out 2>&1"; // 由于该命令的结果不是输出到标准output，因此转存到临时文件再分析
        long startTime = System.currentTimeMillis();
        LinuxUtils.execWaitCommand(commandStr);
        long endTime = System.currentTimeMillis();

        File file = new File("output.out");
        tcpModel.setStatus(0);
        try {
            String line = null;
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((line = reader.readLine()) != null) {

                if (line.contains("succeeded")) {
                    tcpModel.setStatus(1);
                } else {
                    tcpModel.setStatus(0);
                }

            }
            reader.close();
            file.delete(); // 解析完后删除临时文件
        } catch (FileNotFoundException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }

        tcpModel.setIp(ip);
        tcpModel.setPort(port);
        tcpModel.setMonitorName((String) objects[2]);
        tcpModel.setAverage_res_time(startTime - endTime);
        DataQueue.insertToQueue(tcpModel);
    }
}
