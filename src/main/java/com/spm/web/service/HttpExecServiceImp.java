package com.spm.web.service;

import java.net.HttpURLConnection;
import java.net.URL;

import com.spm.web.util.DataQueue;
import com.sprouts.spm_framework.model.HttpModel;
import com.sprouts.spm_framework.utils.Logger;

public class HttpExecServiceImp implements BaseExecService {

    private Logger logger = new Logger();

    /**
     * 使用jdk的HttpURLConnection来对站点做连通性测试
     */
    @Override
    public void analytics(Object... objects) {
        int count = 0;
        int response_code = -1;
        HttpModel httpModel = new HttpModel();
        String urlStr = (String) objects[0];
        if (urlStr == null || urlStr.length() <= 0) {
            logger.warn("Http error: url is null.");
            return;
        }
        // 连接5次，如果5次都连接失败则判断失败，只要有一次连接成功就视为成功
        long start = System.currentTimeMillis();
        while (count < 5) {
            try {
                URL url = new URL(urlStr);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                response_code = connection.getResponseCode();
                if (response_code == 200) {
                    httpModel.setStatus(1);
                    break;
                } else {
                    count++;
                }
                connection.disconnect();
            } catch (Exception ex) {
                count++;
                continue;
            }
        }
        long end = System.currentTimeMillis();

        if (count == 5) {
            httpModel.setStatus(0);
        }

        httpModel.setMonitorName((String) objects[1]);
        httpModel.setStatus_code(response_code);
        httpModel.setAverage_res_time(end - start);
        httpModel.setSite(urlStr);

        DataQueue.insertToQueue(httpModel);
    }


}
