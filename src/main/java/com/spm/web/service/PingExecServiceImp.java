package com.spm.web.service;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.spm.web.util.DataQueue;
import com.sprouts.spm_framework.model.PingModel;
import com.sprouts.spm_framework.utils.LinuxUtils;
import com.sprouts.spm_framework.utils.Logger;

public class PingExecServiceImp implements BaseExecService {

    private Logger logger = new Logger();

    /**
     * ping命令对目标主机ping10次，然后解析命令行返回
     */
    @Override
    public void analytics(Object... objects) {
        String commandStr = "ping " + (String) objects[0] + " -c 10";
        ArrayList<String> datas = LinuxUtils.execCommand(commandStr, 11);
        PingModel pingModel = new PingModel();

        if (datas.size() > 0) {
            float average_res_time = 0.0f;
            boolean[] loss_set = new boolean[datas.size()];

            for (int i = 1; i < datas.size(); i++) {

                String[] data = StringUtils.splitByWholeSeparator(datas.get(i), "time=");

                if (data.length > 0) {
                    average_res_time +=
                            Float.parseFloat(StringUtils.substringBefore(data[1], " ms"));
                    loss_set[i] = true;
                } else {
                    loss_set[i] = false;
                }
            }

            pingModel.setAverage_res_time(average_res_time / (datas.size() - 1));
            pingModel.setPack_loss(calPkgLoss(loss_set));
            pingModel.setPack_receive(calPkgSucc(loss_set));
            pingModel.setLoss_rate(calPkgLossRate(loss_set) * 100);
            pingModel.setPack_send(datas.size() - 1);
            pingModel.setSuccess_rate(calPkgSuccRate(loss_set) * 100);
            pingModel.setStatus(pingModel.getPack_loss() > 0 ? 0 : 1);

        } else {
            pingModel.setAverage_res_time(0);
            pingModel.setPack_loss(0);
            pingModel.setPack_receive(0);
            pingModel.setLoss_rate(0);
            pingModel.setPack_send(0);
            pingModel.setSuccess_rate(0);
            pingModel.setStatus(0);
            logger.warn(String.format("Nothing output for this command: [%s]", commandStr));
        }

        pingModel.setIp((String) objects[0]);
        pingModel.setMonitorName((String) objects[1]);
        DataQueue.insertToQueue(pingModel); // 数据存入本地队列
    }

    /**
     * 计算丢包率
     * 
     * @param loss_set
     * @return
     */
    public float calPkgLossRate(boolean[] loss_set) {
        int loss_pkg = 0;
        for (int i = 0; i < loss_set.length; i++) {
            if (loss_set[i] == false) loss_pkg += 1;
        }
        return loss_pkg / loss_set.length;
    }

    /**
     * 计算成功发送率
     * 
     * @param loss_set
     * @return
     */
    public float calPkgSuccRate(boolean[] loss_set) {
        int succ_pkg = 0;
        for (int i = 0; i < loss_set.length; i++) {
            if (loss_set[i] == true) succ_pkg += 1;
        }
        return succ_pkg / loss_set.length;
    }

    /**
     * 计算成功发送的包数量
     * 
     * @param loss_set
     * @return
     */
    public int calPkgSucc(boolean[] loss_set) {
        int succ_pkg = 0;
        for (int i = 0; i < loss_set.length; i++) {
            if (loss_set[i] == true) succ_pkg += 1;
        }
        return succ_pkg;
    }

    /**
     * 计算丢失的包数量
     * 
     * @param loss_set
     * @return
     */
    public int calPkgLoss(boolean[] loss_set) {
        int loss_pkg = 0;
        for (int i = 0; i < loss_set.length; i++) {
            if (loss_set[i] == false) loss_pkg += 1;
        }
        return loss_pkg;
    }

}
