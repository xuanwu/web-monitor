package com.spm.web.sql;

import java.sql.SQLException;

import com.sprouts.spm_framework.model.HttpModel;
import com.sprouts.spm_framework.model.PingModel;
import com.sprouts.spm_framework.model.TcpModel;
import com.sprouts.spm_framework.model.UdpModel;
import com.sprouts.spm_framework.sql.SQLService;

public class SQLUtils extends SQLService {

    public void insertPing(PingModel pingMod) {
        reInitService();
        String insertSql = "insert into collect_ping values(NULL,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(insertSql);
            preparedStatement.setString(1, pingMod.getMonitorName());
            preparedStatement.setString(2, pingMod.getIp());
            preparedStatement.setInt(3, pingMod.getPack_loss());
            preparedStatement.setInt(4, pingMod.getPack_receive());
            preparedStatement.setInt(5, pingMod.getPack_send());
            preparedStatement.setFloat(6, pingMod.getLoss_rate());
            preparedStatement.setFloat(7, pingMod.getSuccess_rate());
            preparedStatement.setInt(8, pingMod.getStatus());
            preparedStatement.setFloat(9, pingMod.getAverage_res_time());
            preparedStatement.setString(10, getTimeStamp());
            preparedStatement.setLong(11, getTimestampSec());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("SQL ping insertion error!", e);
        }
    }

    public void insertUdp(UdpModel udpMod) {
        reInitService();
        String insertSql = "insert into collect_udp values(NULL,?,?,?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(insertSql);
            preparedStatement.setString(1, udpMod.getMonitorName());
            preparedStatement.setString(2, udpMod.getIp());
            preparedStatement.setInt(3, udpMod.getPort());
            preparedStatement.setInt(4, udpMod.getStatus());
            preparedStatement.setFloat(5, udpMod.getAverage_res_time());
            preparedStatement.setString(6, getTimeStamp());
            preparedStatement.setLong(7, getTimestampSec());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("SQL udp insertion error!", e);
        }
    }

    public void insertTcp(TcpModel tcpMod) {
        reInitService();
        String insertSql = "insert into collect_tcp values(NULL,?,?,?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(insertSql);
            preparedStatement.setString(1, tcpMod.getMonitorName());
            preparedStatement.setString(2, tcpMod.getIp());
            preparedStatement.setInt(3, tcpMod.getPort());
            preparedStatement.setInt(4, tcpMod.getStatus());
            preparedStatement.setFloat(5, tcpMod.getAverage_res_time());
            preparedStatement.setString(6, getTimeStamp());
            preparedStatement.setLong(7, getTimestampSec());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("SQL tcp insertion error!", e);
        }
    }

    public void insertHttp(HttpModel httpMod) {
        reInitService();
        String insertSql = "insert into collect_http values(NULL,?,?,?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(insertSql);
            preparedStatement.setString(1, httpMod.getMonitorName());
            preparedStatement.setString(2, httpMod.getSite());
            preparedStatement.setInt(3, httpMod.getStatus());
            preparedStatement.setInt(4, httpMod.getStatus_code());
            preparedStatement.setFloat(5, httpMod.getAverage_res_time());
            preparedStatement.setString(6, getTimeStamp());
            preparedStatement.setLong(7, getTimestampSec());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("SQL http insertion error!", e);
        }
    }

    public void updateMonitorStatus(String monitorname, int status) {
        reInitService();
        String updateSql = "update list_spm_web set status=? where monitorname=?";
        try {
            preparedStatement = connection.prepareStatement(updateSql);
            preparedStatement.setInt(1, status);
            preparedStatement.setString(2, monitorname);
            preparedStatement.executeUpdate();
            logger.info("monitorstatus update success!");
        } catch (SQLException e) {
            logger.error("SQL update error!", e);
        }
    }

}
