package com.spm.web.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.spm.web.sql.SQLUtils;
import com.sprouts.spm_framework.model.HttpModel;
import com.sprouts.spm_framework.model.PingModel;
import com.sprouts.spm_framework.model.TcpModel;
import com.sprouts.spm_framework.model.UdpModel;
import com.sprouts.spm_framework.utils.Logger;

/**
 * 数据队列，监控数据暂存到队列中，然后一次性发送，避免数据库连接频繁
 * 
 * @author howson
 * 
 */
public class DataQueue {

    private static BlockingQueue<HttpModel> httpQueue = new ArrayBlockingQueue<HttpModel>(1000);
    private static BlockingQueue<PingModel> pingQueue = new ArrayBlockingQueue<PingModel>(1000);
    private static BlockingQueue<TcpModel> tcpQueue = new ArrayBlockingQueue<TcpModel>(1000);
    private static BlockingQueue<UdpModel> udpQueue = new ArrayBlockingQueue<UdpModel>(1000);
    private static SQLUtils sqlUtils = new SQLUtils();
    private static Logger logger = new Logger();

    /**
     * 数据暂存到队列中
     * 
     * @param data
     */
    public static <T> void insertToQueue(T data) {
        try {
            if (data instanceof HttpModel) {
                httpQueue.put((HttpModel) data);
            } else if (data instanceof PingModel) {
                pingQueue.put((PingModel) data);
            }
            if (data instanceof TcpModel) {
                tcpQueue.put((TcpModel) data);
            }
            if (data instanceof UdpModel) {
                udpQueue.put((UdpModel) data);
            }
        } catch (InterruptedException e) {
            logger.error("insert to data queue error:", e);
        }
    }

    /**
     * 数据入库
     */
    public static void saveToDatabase() {
        sqlUtils.connectDatabase();

        for (HttpModel http : httpQueue) {
            sqlUtils.insertHttp(http);
        }
        for (PingModel ping : pingQueue) {
            sqlUtils.insertPing(ping);
        }
        for (TcpModel tcp : tcpQueue) {
            sqlUtils.insertTcp(tcp);
        }
        for (UdpModel udp : udpQueue) {
            sqlUtils.insertUdp(udp);
        }

        sqlUtils.close();
    }
}
