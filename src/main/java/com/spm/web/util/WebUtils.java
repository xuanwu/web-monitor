package com.spm.web.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.lang.StringUtils;

import com.spm.web.handler.Watcher;
import com.spm.web.service.HttpExecServiceImp;
import com.spm.web.service.PingExecServiceImp;
import com.spm.web.service.TcpExecServiceImp;
import com.spm.web.service.UdpExecServiceImp;
import com.sprouts.spm_framework.utils.AsyncTaskUtils;
import com.sprouts.spm_framework.utils.Logger;

public class WebUtils {
    @SuppressWarnings("rawtypes")
    public static Map<String, ScheduledFuture> localTasksMap =
            new HashMap<String, ScheduledFuture>(); // 本地任务Map，用来取消任务和创建任务
    private static Logger logger = new Logger();

    @SuppressWarnings("rawtypes")
    public static void insertToTaskMap(String name, ScheduledFuture task) {
        try {
            localTasksMap.put(name, task);
        } catch (Exception e) {
            logger.error("Error in inserting element from task map:", e);
        }
    }

    public static void removeFromTaskMap(String name) {
        try {
            ScheduledFuture cancelTask = getValue(name);
            cancelTask.cancel(true);
            localTasksMap.remove(name);
        } catch (Exception e) {
            logger.error("Error in removing element from task map:", e);
        }
    }

    public static boolean containsValue(String key) {
        boolean find = false;
        try {
            find = localTasksMap.containsKey(key);
        } catch (Exception e) {
            logger.error("Error in finding element in task map:", e);
        }
        return find;
    }

    @SuppressWarnings("rawtypes")
    public static ScheduledFuture getValue(String key) {
        ScheduledFuture future = null;
        try {
            future = localTasksMap.get(key);
        } catch (Exception e) {
            logger.error("Error in getting element from task map:", e);
        }
        return future;
    }

    /**
     * 把任务持久化到本地文件，以方便程序崩溃时恢复任务
     */
    public static void cachedToLocal(Object... objects) {
        String line = "";
        for (Object obj : objects) {
            line += (String) obj + "/";
        }
        writeCached(line);
    }

    public static void writeCached(String cache) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("cached.dat", true));
            writer.write(cache);
            writer.write("\n");
            writer.close();
        } catch (IOException e) {
            logger.error("write cache error:", e);
        }
    }

    /**
     * 程序重启时恢复任务
     */
    public static void retrieveTasks(AsyncTaskUtils asyncHandler) {
        try {
            File file = new File("cached.dat");
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader("cached.dat"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    if (line.contains("\n")) {
                        line.replace("\n", "");
                    }

                    String[] cols = StringUtils.split(line, "/");
                    if ((!line.isEmpty()) && cols.length > 0) {
                        Watcher watcher = getWatcher(cols);
                        if (watcher != null) {
                            asyncHandler.dispatchScheduleTask(watcher, 0,
                                    Integer.parseInt(cols[cols.length - 1]));
                        }
                    }
                }
                reader.close();
            }
        } catch (FileNotFoundException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    public static Watcher getWatcher(String[] cols) {
        switch (cols[0]) {
            case "http":
                return new Watcher(new HttpExecServiceImp(), cols[1], cols[2]);
            case "ping":
                return new Watcher(new PingExecServiceImp(), cols[1], cols[2]);
            case "tcp":
                return new Watcher(new TcpExecServiceImp(), cols[1], cols[2], cols[3]);
            case "udp":
                return new Watcher(new UdpExecServiceImp(), cols[1], cols[2], cols[3]);
            default:
                return null;
        }
    }
}
