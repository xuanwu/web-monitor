package com.spm.web.util;

import java.util.ArrayList;
import java.util.List;

import com.spm.web.model.MonitorStatus;

public class StaticConstants {

    public static List<MonitorStatus> PING_TASK_LIST = new ArrayList<MonitorStatus>();
    public static List<MonitorStatus> UDP_TASK_LIST = new ArrayList<MonitorStatus>();
    public static List<MonitorStatus> TCP_TASK_LIST = new ArrayList<MonitorStatus>();
    public static List<MonitorStatus> HTTP_TASK_LIST = new ArrayList<MonitorStatus>();
}
