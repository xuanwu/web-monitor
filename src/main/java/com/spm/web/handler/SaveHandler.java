package com.spm.web.handler;

import com.spm.web.util.DataQueue;

/**
 * 每分钟扫描一次数据队列，把数据存到数据库
 * 
 * @author howson
 * 
 */
public class SaveHandler implements Runnable {

    @Override
    public void run() {
        DataQueue.saveToDatabase();
    }

}
