package com.spm.web.handler;

import com.spm.web.service.BaseExecService;

public class Watcher implements Runnable {

    private BaseExecService baseService;
    private Object[] objects;

    public Watcher(BaseExecService baseService, Object... objects) {
        this.baseService = baseService;
        this.objects = objects;
    }

    @Override
    public void run() {
        baseService.analytics(objects);
    }
}
