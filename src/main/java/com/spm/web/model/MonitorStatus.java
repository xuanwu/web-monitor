package com.spm.web.model;

import com.sprouts.spm_framework.enums.LifeCycleState;



public class MonitorStatus {

    private Integer id;
    private String hostname; // 监控主机名 或 IP地址
    private String created_time; // 创建的时间
    private Integer status; // 当前状态
    private String nodeIP; // 监控IP地址
    private String nodesystem; // 监控的操作系统
    private String type; // 监控类型 ：1 : ping 2: tcp 3：udp 4:http
    private Double timestamp; // 延迟时间，毫秒
    private Integer frequency; // 监控频率秒数
    private String user; // 监控用户，可为空
    private String monitorname; // 监控名称，即实例名称
    private String monitorMethod; // http的参数 监控方式
    private int port; // udp,tcp监控端口
    private int avg_res_time; // udp_udp_ 平均响应时间
    private String udp_status; // udp json 数据
    private String tcp_status; // tcp json数据
    private String ping_status; // ping的状态，
    private LifeCycleState lifeCycleState;

    public MonitorStatus() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }


    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNodeIP() {
        return nodeIP;
    }

    public void setNodeIP(String nodeIP) {
        this.nodeIP = nodeIP;
    }

    public String getNodesystem() {
        return nodesystem;
    }

    public void setNodesystem(String nodesystem) {
        this.nodesystem = nodesystem;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMonitorMethod() {
        return monitorMethod;
    }

    public void setMonitorMethod(String monitorMethod) {
        this.monitorMethod = monitorMethod;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMonitorname() {
        return monitorname;
    }

    public void setMonitorname(String monitorname) {
        this.monitorname = monitorname;
    }

    public Double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    public String getPing_status() {
        return ping_status;
    }

    public void setPing_status(String ping_status) {
        this.ping_status = ping_status;
    }

    public String getUdp_status() {
        return udp_status;
    }

    public void setUdp_status(String udp_status) {
        this.udp_status = udp_status;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getAvg_res_time() {
        return avg_res_time;
    }

    public void setAvg_res_time(int avg_res_time) {
        this.avg_res_time = avg_res_time;
    }

    public String getTcp_status() {
        return tcp_status;
    }

    public void setTcp_status(String tcp_status) {
        this.tcp_status = tcp_status;
    }

    public LifeCycleState getLifeCycleState() {
        return lifeCycleState;
    }

    public void setLifeCycleState(LifeCycleState lifeCycleState) {
        this.lifeCycleState = lifeCycleState;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
        result = prime * result + ((monitorname == null) ? 0 : monitorname.hashCode());
        result = prime * result + ((nodeIP == null) ? 0 : nodeIP.hashCode());
        result = prime * result + port;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        MonitorStatus other = (MonitorStatus) obj;
        if (frequency == null) {
            if (other.frequency != null) return false;
        } else if (!frequency.equals(other.frequency)) return false;
        if (monitorname == null) {
            if (other.monitorname != null) return false;
        } else if (!monitorname.equals(other.monitorname)) return false;
        if (nodeIP == null) {
            if (other.nodeIP != null) return false;
        } else if (!nodeIP.equals(other.nodeIP)) return false;
        if (port != other.port) return false;
        if (type == null) {
            if (other.type != null) return false;
        } else if (!type.equals(other.type)) return false;
        return true;
    }



}
