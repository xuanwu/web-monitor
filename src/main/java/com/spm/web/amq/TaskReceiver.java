package com.spm.web.amq;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.spm.web.handler.Watcher;
import com.spm.web.service.HttpExecServiceImp;
import com.spm.web.service.PingExecServiceImp;
import com.spm.web.service.TcpExecServiceImp;
import com.spm.web.service.UdpExecServiceImp;
import com.spm.web.util.WebUtils;
import com.sprouts.spm_framework.amq.AmqConfig;
import com.sprouts.spm_framework.amq.AmqReceiver;
import com.sprouts.spm_framework.amq.message.WebTaskMessage;
import com.sprouts.spm_framework.utils.AsyncTaskUtils;
import com.sprouts.spm_framework.utils.ConfigUtils;
import com.sprouts.spm_framework.utils.Logger;
import com.sprouts.spm_framework.utils.SPMConstants;

public class TaskReceiver {
    public static AmqReceiver receiver;
    private Logger logger = new Logger();
    AsyncTaskUtils asynHandler = null;

    public void initReceiver() {
        asynHandler = AsyncTaskUtils.getInstance();
        AmqConfig config =
                AmqConfig.getDefaultConfig(ConfigUtils.getValue(SPMConstants.WEB_MONITOR_BROKER),
                        ConfigUtils.getValue(SPMConstants.WEB_MONITOR_QUEUE), WebTaskMessage.class,
                        new WebMonitorListener());
        receiver = AmqReceiver.initAmqReceiver(config);
        receiver.receive();
    }

    /**
     * ActiveMQ接收消息的监听器
     * 
     * @author howson
     * 
     */
    class WebMonitorListener implements MessageListener {

        @Override
        public void onMessage(Message arg0) {
            try {
                if (arg0 != null) {

                    WebTaskMessage message = new WebTaskMessage();
                    message = (WebTaskMessage) message.parseMapMsgToBaseMsg((MapMessage) arg0);
                    String key =
                            message.getMonitorName() + "/" + message.getDestination() + "/"
                                    + message.getType().type;
                    // 判断消息是要取消线程还是创建线程
                    switch (message.getType()) {
                        case HTTP:
                            WebUtils.insertToTaskMap(key, asynHandler.dispatchScheduleTask(
                                    new Watcher(new HttpExecServiceImp(), message.getDestination(),
                                            message.getMonitorName()), 0, message.getFrequency()));
                            WebUtils.cachedToLocal("http", message.getDestination(),
                                    message.getMonitorName(), message.getFrequency());
                            break;
                        case PING:
                            WebUtils.insertToTaskMap(key, asynHandler.dispatchScheduleTask(
                                    new Watcher(new PingExecServiceImp(), message.getDestination(),
                                            message.getMonitorName()), 0, message.getFrequency()));
                            WebUtils.cachedToLocal("ping", message.getDestination(),
                                    message.getMonitorName(), message.getFrequency());
                            break;
                        case TCP:
                            WebUtils.insertToTaskMap(key, asynHandler.dispatchScheduleTask(
                                    new Watcher(new TcpExecServiceImp(), message.getDestination(),
                                            message.getPort(), message.getMonitorName()), 0,
                                    message.getFrequency()));
                            WebUtils.cachedToLocal("tcp", message.getDestination(),
                                    message.getPort(), message.getMonitorName(),
                                    message.getFrequency());
                            break;
                        case UDP:
                            WebUtils.insertToTaskMap(key, asynHandler.dispatchScheduleTask(
                                    new Watcher(new UdpExecServiceImp(), message.getDestination(),
                                            message.getPort(), message.getMonitorName()), 0,
                                    message.getFrequency()));
                            WebUtils.cachedToLocal("udp", message.getDestination(),
                                    message.getPort(), message.getMonitorName(),
                                    message.getFrequency());
                            break;
                        case CANCEL:
                            WebUtils.removeFromTaskMap(key);
                            break;
                        default:
                            break;
                    }
                } else {
                    logger.error("The message received is null.");
                }
            } catch (Exception e) {
                logger.error("", e);
            }
        }
    }

}
