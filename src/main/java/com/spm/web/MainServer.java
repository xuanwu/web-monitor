package com.spm.web;

import com.spm.web.amq.TaskReceiver;
import com.spm.web.handler.SaveHandler;
import com.spm.web.util.WebUtils;
import com.sprouts.spm_framework.utils.AsyncTaskUtils;
import com.sprouts.spm_framework.utils.ConfigUtils;

public class MainServer {

    public static void main(String[] args) {
        ConfigUtils.initExternalConfig("../config/site-config.xml");

        // 初始化线程池并恢复本地任务
        AsyncTaskUtils asyncHandler = AsyncTaskUtils.getInstance();
        WebUtils.retrieveTasks(asyncHandler);

        // 初始化任务接收器AmqReceiver
        TaskReceiver taskReceiver = new TaskReceiver();
        taskReceiver.initReceiver();

        // 初始化数据入库线程，每分钟扫描一次数据队列
        asyncHandler.dispatchScheduleTask(new SaveHandler(), 1, 1);

    }

}
