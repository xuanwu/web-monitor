package com.spm.web.udptest;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class UDPClientTest {
	public static void main(String[] args) throws Exception {
		
		DatagramSocket socket = new DatagramSocket(9999);
		byte[] buf = "halo".getBytes();
		byte[] buf1 = new byte[1024];
		DatagramPacket packet = new DatagramPacket(buf1,0,buf1.length,new InetSocketAddress("125.216.243.183",20030));
		DatagramPacket packet_rev = new DatagramPacket(buf1,buf1.length);
		socket.send(packet);
		System.out.println(socket.isConnected());
		socket.close();
		
	}
	
}

