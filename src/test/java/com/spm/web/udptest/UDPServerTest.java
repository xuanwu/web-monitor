package com.spm.web.udptest;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPServerTest {

	public static void main(String[] args) throws Exception {
		
		byte[] buf = new byte[1024];
		
		DatagramSocket socket = new DatagramSocket(20030);
		DatagramPacket packet = new DatagramPacket(buf,buf.length);
		while(true){
			socket.receive(packet);
			System.out.println(new String(buf,0,buf.length));
		}
				
	}
}

